# Teorcode Project
Шаблон отсюда https://github.com/tomorrow10/spring-boot-react-template

## Гайд по запуску через сборку jar

 ```sh
 mvn clean install # в корневой папке
 cd backend
 mvn clean package spring-boot:repackage
 java -jar target/some-app-backend-0.1-SNAPSHOT.jar 
 ```

 Порт 8181, http://127.0.0.1:8181, настраивается в файле **backend/src/main/resources/application.yml**
 
 
 ### Гайд по запуску собранного jar в докере
 - актуальный Dockerfile - **dockerfiles/Dockerfile**
 - собираем образ `docker build -t jarapp:release <path to Dockerfile>`
 - запуск контейнера с этим образом `docker run --name <name you want> -d -p 8181:<port you want> jarapp:release`
 - список запущенных контейнеров `docker ps`
 - остановка `docker stop <name you want>`
 - удаление `docker rm <name you want>`

## Сhangelogs

<details>
  <summary>DevOps changelog</summary>
  
  ### version 0.1.0
  - создан pipeline в jenkins, собирает по настройкам jenkinsfiles/docker_build.jenkins шаблонный проект
  ### version 0.1.1
  - теперь pipeline в jenkins собирает итоговый jar
  - добавлен триггер на коммит в ветку main 
  ### version 0.1.2 
  - добавлена автосборка в докер образ по Dockerfile 
  - добавлен автозапуск контейнера сразу после сборки
  ### version 0.1.3
  - теперь изменения в докерфайле запускают билд только этапов с докером
  - изменения в readme вообще не запускают никаких билдов
  - пофикшен этап сборки jenkinsfile, раньше забывали собирать parent project 
  ### планы
  - сонаркуб
  - ветки
  - улучшить feedback
  - улучшить jenkinsfile
</details>



