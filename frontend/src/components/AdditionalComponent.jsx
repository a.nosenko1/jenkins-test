import * as React from "react";
const AdditionalComponent =  function (props) {

    return (
    <div>
        {props.description}
        <input defaultValue={props.default} onChange={e=>props.function(e.target.value)}/>
    </div>
    );
}
export default AdditionalComponent