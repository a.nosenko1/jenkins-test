import * as React from "react";
const SendButton =  function (props) {
  function Send(){  
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({Data: props.matrix,Operation:props.oper,Alphabet:props.alphabet})

    };
    //Адрес сервера
    fetch('https://reqres.in/api/posts', requestOptions)
}
   return( <div>
        <button onClick={Send}>{props.name}</button>
        
    </div>
    )
}
export default SendButton