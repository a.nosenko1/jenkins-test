import * as React from "react";
import './Mat.css';
const SizeMatrix =  function (props) {
    const[maxRows]=[12]
    return (
        <div>
        <input
            className="Inputclass"
            type="number"
            defaultValue={2}
            onChange={e => {
                const rows = parseInt(e.target.value)
                 if (1 <= rows && rows <= maxRows) {
                    props.setMatrixSize(prevSize => ({
                    ...prevSize,
                     rows: rows,
                    }))
                }
                
            }}
        />
        X
        <input
            className="Inputclass"
            type="number"
            defaultValue={2}
            onChange={e => {
                const columns = parseInt(e.target.value)
                 if (1 <= columns && columns <= maxRows) {
                    props.setMatrixSize(prevSize => ({
                    ...prevSize,
                    columns: columns,
                    }))
                }
            }}
        />
      </div>
    );
}
export default SizeMatrix